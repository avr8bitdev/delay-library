/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DELAY_FUNCTIONS_DELAY_HELPER_FUNCTIONS_H_
#define DELAY_FUNCTIONS_DELAY_HELPER_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

typedef enum
{
	nano_s,
	micro_s,
	milli_s,
	seconds
} Time_prefix;

// req_time = number of iterations * (LOOP_CLCKS * 10^9) / F_CPU
#define MIN_ALLOWED_NANO_S 1.0   * 3 * 1000000000UL / F_CPU
#define MAX_ALLOWED_NANO_S 255.0 * 3 * 1000000000UL / F_CPU

#define MIN_ALLOWED_MICRO_S 1.0     * 4 * 1000000UL / F_CPU
#define MAX_ALLOWED_MICRO_S 65535.0 * 4 * 1000000UL / F_CPU

#define MIN_ALLOWED_MILLI_S 1.0     * 4 * 1000UL / F_CPU
#define MAX_ALLOWED_MILLI_S 65535.0 * 4 * 1000UL / F_CPU

void delay_by_nano(register u8 u8IterationsCpy);
void delay_by_micro(register u16 u16IterationsCpy);
void delay_by_milli(register u16 u16IterationsCpy);
void delay_by(f64 f64delTimeCpy, Time_prefix enumTimePrefixCpy);

inline void delay_by_nano(register u8 u8IterationsCpy)
{ // cycles = 3

	asm volatile (
			"dec %0 \n\t"   // dec %0 (ex: dec r16) -> 1 clock cycle
			"brne .-4 \n\t" // jmp if not equal to previous instruction (it should be at offset -4 from whatever next instruction) -> 2 clock cycles
			:"=r"(u8IterationsCpy) // output is the variable 'itr' (addressed as a register by this constraint)
			:"0"(u8IterationsCpy)  // input is the variable 'itr' at argument 0
			);
}

inline void delay_by_micro(register u16 u16IterationsCpy)
{ // cycles = 4

	asm volatile (
			"sbiw %0,1 \n\t" // (u16)%0 - 1 (ex: sbiw r16, 1) -> 2 clock cycle
			"brne .-4 \n\t"  // jmp if not equal to previous instruction (it should be at offset -4 from whatever next instruction) -> 2 clock cycles
			:"=w"(u16IterationsCpy) // output is the variable 'itr' (addressed as a register by this constraint)
			:"0"(u16IterationsCpy)  // input is the variable 'itr' at argument 0
			);
}

inline void delay_by_milli(register u16 u16IterationsCpy)
{ // cycles = 4

	asm volatile (
			"sbiw %0,1 \n\t" // (u16)%0 - 1 (ex: sbiw r16, 1) -> 2 clock cycle
			"brne .-4 \n\t"  // jmp if not equal to previous instruction (it should be at offset -4 from whatever next instruction) -> 2 clock cycles
			:"=w"(u16IterationsCpy) // output is the variable 'itr' (addressed as a register by this constraint)
			:"0"(u16IterationsCpy)  // input is the variable 'itr' at argument 0
			);
}

inline void delay_by(f64 f64delTimeCpy, Time_prefix enumTimePrefixCpy)
{
	/*
	   Three tricks are deployed/used in this function allowing it to be compiled into ~3-6 assembly lines only!
        1) Compiler optimization must be enabled.
	    2) The code path must always be defined and specific, if and only if the input arguments are hard-coded by the programmer.
	    3) These functions must exist inside the target compilation unit, hence they're implemented in this .h file
	    4) (Optional) 'inline' attribute hints to the compiler that (obviously) it's preferred to inline this function, hence no pro/epi-logue.


	   // example for nano seconds:
	   // ------------------------
	   //
	   //  let: F_CPU = 16M         (MCU clock frequency in [Hertz]),
	   //       T_CLK = 1 / F_CPU   (time of 1 clock cycle in [seconds]),
	   //       DELAY_LOOP_CLKS = 3 (number of clock cycles wasted in the delay loop)
	   //  therefore:
	   //     time of 1 iteration  = (DELAY_LOOP_CLCKS * T_CLK) * 10^9
	   //     so, for a given time 'req_time',
	   //           number of iterations = req_time / time of 1 iteration
	   //           number of iterations = req_time / [ (DELAY_LOOP_CLCKS * T_CLK) * 10^9 ]
	   //           number of iterations = req_time * F_CPU / (LOOP_CLCKS * 10^9)
	   // ------------------------
	   // 10^9 may be replaced by the inverse of required time prefix
	   // i.e nano  -> 10^9, micro -> 10^6, etc...
	*/

	f64 number_of_iterations = f64delTimeCpy * F_CPU;

	switch (enumTimePrefixCpy)
	{
		case nano_s:
			number_of_iterations /= 3.0 * 1000000000UL;
			if ( (u8)number_of_iterations )
				delay_by_nano( (u8)number_of_iterations );
		break;

		case micro_s:
			number_of_iterations /= 4.0 * 1000000UL;
			if ( (u16)number_of_iterations )
				delay_by_micro( (u16)number_of_iterations );
		break;

		case milli_s:
		{
			number_of_iterations /= 4.0 * 1000UL;

			register u16 multiple_itr = (u64)number_of_iterations / 65535;
			register u16 remaining_itr = (u64)number_of_iterations % 65535;

			if (multiple_itr)
			{
				while (multiple_itr--)
					delay_by_milli(65535);
			}

			if (remaining_itr)
				delay_by_milli(remaining_itr);
		}
		break;

		case seconds:
		{
			number_of_iterations /= 4.0;

			register u16 multiple_itr = (u64)number_of_iterations / 65535;
			register u16 remaining_itr = (u64)number_of_iterations % 65535;


			if (multiple_itr)
			{
				while (multiple_itr--)
					delay_by_milli(65535);
			}

			if (remaining_itr)
				delay_by_milli(remaining_itr);
		}
		break;
	}

}


#endif /* DELAY_FUNCTIONS_DELAY_HELPER_FUNCTIONS_H_ */

